#![allow(dead_code)]
#![allow(unused_variables)]
use cid::Cid;
use ipfs_unixfs::file::adder::FileAdder;

use std::error::Error;
use std::fmt;
use std::env;
use std::fs;
use std::process;


fn main() {

    let config = Config::new(env::args()).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    if let Err(e) = self::run(config) {
        eprintln!("Application error: {}", e);

        process::exit(1);
    }

}

fn run(config: Config) -> Result<String, Box<dyn Error>> {
    
    let mut adder = FileAdder::default();

    let mut stats = Stats::default();
    
    let mut input = 0;

    let contents = fs::read(&config.filename)?;
    

    let mut total = 0;

    while total < contents.len() {
        let (blocks, consumed) = adder.push(&contents[total..]);
        stats.process(blocks);

        input += consumed;
        total += consumed;
    }

    let blocks = adder.finish();
    stats.process(blocks);

    eprintln!("{}", stats);

    Ok(format!("{}",stats))
}

pub struct Config {
    pub filename: String,
}

impl Config {
    /// Make a new instance

    pub fn new(mut args: env::Args) -> Result<Config, &'static str> {
        //Consume the args given to config by env::args() -- expecting [~/..../rust_cid_npm <FILENAME>]
        args.next(); // skip calling arg/program

        let filename = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a file name.\nExpected to call this executable with path of a file.\n\nExample:\n
            rust_cid_npm text.txt\nExiting..."),
        };

        Ok(Config {
            filename,
        })
    }
}

#[derive(Default)]
struct Stats {
    blocks: usize,
    block_bytes: u64,
    last: Option<Cid>,
}

impl fmt::Display for Stats {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        let cidv0 = self.last.as_ref().unwrap();
        let cidv1 = Cid::new_v1(cid::Codec::DagProtobuf, cidv0.hash().to_owned());
        write!(
            fmt,
            "{{\"CIDv0\":\"{}\",\"CIDv1\":\"{}\"}}",
            cidv0,
            cidv1,
            // self.blocks,
            // self.block_bytes,
        )
    }
}

impl Stats {
    fn process<I: Iterator<Item = (Cid, Vec<u8>)>>(&mut self, new_blocks: I) {
        for (cid, block) in new_blocks {
            self.last = Some(cid);
            self.blocks += 1;
            self.block_bytes += block.len() as u64;
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_cid_cli(){
        // Known test case for simple string "abcdefg\n" as a Vec<u8> -- tool that can help: https://onlineunicodetools.com/convert-unicode-to-utf8  -- note that u8 is ecoded in decimal!!! "abcdefg\n" = vec![97, 98, 99, 100, 101, 102, 103, 10]
        // NOTE: the expects a test file "./tests/test.txt" assuming your working directory contains `Cargo.toml`! This file must contain exactly this string without quotes: "abcdefg\n" (\n is newline, unix like, UTF8 = `10` in decimal)

        let test_file = Config {
            filename: String::from("tests/test.txt")
        };

        let cidv0 = "QmYhMRHZAceoM8L21yqBcJjk1TXasZzHw4g1qzTSHBLq1c";
        let cidv1 = "bafybeiez4kcddcze2tyxsrxzp3lbtx6jyue3mhbg4uo2t2mmdfywwp3sxm";

        let expected = String::from(format!("{{\"CIDv0\":\"{}\",\"CIDv1\":\"{}\"}}",cidv0,cidv1));

        let result = run(test_file).unwrap();

        assert_eq!(expected, result);    
    }
}
