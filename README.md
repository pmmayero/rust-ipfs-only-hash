# rust-ipfs-only-hash

Rust implementation and Debian package file that generates the IPFS CID V0 and CID V1 of a file from the cli.  
Adapted from https://gitlab.com/NukeManDan/rust_cid_npm   
Debian package generation procedure adapted from https://blog.karmacomputing.co.uk/how-to-create-deb-package-from-rust/   

## Name
rust-ipfs-only-hash

## Description
Rust implmentation used to generate CID V0 and CID V1 of files without having to run an IPFS daemon.

## Usage
### Prerequisite
Rust and Cargo installed

### As a script
- Clone this repo 
- Run the script as ``` cargo run path-to-file/filename ```  

### To build a Debian Package
- Clone this repo
- Install cargo deb by running the command ```cargo install cargo-deb```  
- Create a deb package by running ```cargo deb ```  
- Check /target/debian for rust-ipfs-only-hash_0.1.0_amd64.deb  
- Install debain file by running ``` sudo dpkg -i ~/target/debian/rust-ipfs-only-hash_0.1.0_amd64.deb ```   
- To use the debian package, execute ``` rust-ipfs-only-hash path-to-file/filename ```  
Note: This is to be done on a Debian installation  

## Authors and acknowledgment
https://gitlab.com/NukeManDan
